#ifndef MONLIB_UTIL_DYNARRAY_H
#define MONLIB_UTIL_DYNARRAY_H

#include <cstddef>
#include <memory>
#include <stdexcept>
#include <type_traits>
#include <utility>

#include <gsl/gsl>

namespace Monlib::Util {
    // Represents an owning span.
    // Naming convenctions changed to match that of other memory primitives.
    template <class T>
    using minimally_templated_unique_ptr = std::unique_ptr<T>;
    template <class T, template <class> class owner = minimally_templated_unique_ptr>
    class dynarray {
    public:
        using element_type = T;
        using value_type = std::remove_cv_t<T>;
        using index_type = std::ptrdiff_t;
        using difference_type = std::ptrdiff_t;
        using reference = element_type&;
        using const_reference = const element_type&;
        using pointer = element_type*;
        using const_pointer = const element_type*;
        using iterator = T*;
        using const_iterator = const T*;
        using reverse_iterator = std::reverse_iterator<iterator>;
        using const_reverse_iterator = std::reverse_iterator<const_iterator>;

        dynarray() : _length{0}, _data{nullptr} {}
        dynarray(index_type length, owner<T[]> data) :
            _length{length},
            _data{std::move(data)} {}
        template <class U, typename = std::enable_if_t<sizeof(U) == sizeof(T)>>
        dynarray(dynarray<U, owner> other) :
            _length(other._length),
            _data(std::move(other._data)) {}
        dynarray(const dynarray&) = default;
        dynarray(dynarray&&) = default;
        dynarray& operator=(const dynarray&) = default;
        dynarray& operator=(dynarray&&) = default;

        reference at(index_type pos) const {
            if (!(pos < size()))
                throw std::out_of_range("pos is not valid!");
            else
                return operator[](pos);
        }
        reference operator[](index_type pos) const noexcept { return data()[pos]; }
        reference front() const noexcept { return data()[0]; }
        reference back() const noexcept { return _data()[_length - 1]; }
        T* data() const noexcept { return &_data[0]; }
        [[nodiscard]] bool empty() const noexcept { return _length == 0; }
        index_type size() const noexcept { return _length; }
        index_type max_size() const noexcept {
            return std::numeric_limits<index_type>::max();
        }
        void swap(dynarray& other) noexcept {
            std::swap(this->_length, other._length);
            std::swap(this->_data, other._data);
        }
        iterator begin() const noexcept { return data(); }
        const_iterator cbegin() const noexcept { return data(); }
        iterator end() const noexcept { return data() + size(); }
        const_iterator cend() const noexcept { return data() + size(); }
        reverse_iterator rbegin() const { return reverse_iterator(end()); }
        const_reverse_iterator crbegin() const {
            return const_reverse_iterator{end()};
        }
        reverse_iterator rend() const { return reverse_iterator{begin()}; }
        const_reverse_iterator crend() const {
            return const_reverse_iterator{begin()};
        }
        const owner<T[]>& get() const { return _data; }
        owner<T[]>& get() { return _data; }

    private:
        index_type _length;
        owner<T[]> _data;

        template <class, template <class> class>
        friend class dynarray;
    };

    template <class T, template <class> class O>
    bool operator==(dynarray<T, O> lhs, std::nullptr_t rhs) {
        return lhs.get() == rhs;
    }

    template <class T, template <class> class O>
    bool operator==(std::nullptr_t lhs, dynarray<T, O> rhs) {
        return rhs == lhs;
    }

    template <class T, template <class> class O>
    bool operator==(dynarray<T, O> lhs, dynarray<T, O> rhs) {
        using std::begin, std::end;
        return std::equal(begin(lhs), end(lhs), begin(rhs), end(rhs));
    }

    template <class T, template <class> class O>
    bool operator!=(dynarray<T, O> lhs, dynarray<T, O> rhs) {
        return !(lhs == rhs);
    }

    template <class T, template <class> class O>
    bool operator!=(dynarray<T, O> lhs, std::nullptr_t rhs) {
        return lhs.get() != rhs;
    }
    template <class T, template <class> class O>
    bool operator!=(std::nullptr_t lhs, dynarray<T, O> rhs) {
        return rhs != lhs;
    }

    template <class T>
    void swap(dynarray<T>& lhs, dynarray<T>& rhs) noexcept {
        return lhs.swap(rhs);
    }

    template <class T>
    dynarray<T> make_dynarray(typename dynarray<T>::index_type size) {
        return {size, std::make_unique<T[]>(gsl::narrow<std::size_t>(size))};
    }

    template <class T>
    using minimally_templated_shared_ptr = std::shared_ptr<T>;
    template <class T>
    using shared_dynarray = dynarray<T, minimally_templated_shared_ptr>;
    template <class T>
    using unique_dynarray = dynarray<T, minimally_templated_unique_ptr>;

    template <class T>
    shared_dynarray<T> make_shared_dynarray(typename dynarray<T>::index_type size) {
        return {size, std::shared_ptr<T[]>{new T[size]}};
    }

    template <class T>
    shared_dynarray<T> slice_dynarray(shared_dynarray<T> data,
        typename shared_dynarray<T>::index_type start,
        typename shared_dynarray<T>::index_type len) {
        return {len, std::shared_ptr<T[]>{data.get(), &data[start]}};
    }

    template <class T, template <class> class O>
    shared_dynarray<std::remove_cv_t<T>> copy_shared_dynarray(
        const dynarray<T, O>& original) {
        using std::begin, std::end;
        auto copy = make_shared_dynarray<std::remove_cv_t<T>>(original.size());
        std::copy(begin(original), end(original), begin(copy));
        return copy;
    }

    template <class T, template <class> class O>
    dynarray<std::remove_cv_t<T>> copy_unique_dynarray(
        const dynarray<T, O>& original) {
        using std::begin, std::end;
        auto copy = make_dynarray<std::remove_cv_t<T>>(original.size());
        std::copy(begin(original), end(original), begin(copy));
        return copy;
    }
}

#endif  // MONLIB_UTIL_DYNARRAY_H
