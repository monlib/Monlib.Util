#ifndef MONLIB_UTIL_EASYRAGE_H
#define MONLIB_UTIL_EASYRAGE_H

#include <type_traits>

namespace Monlib::Util {
    template <class TBegin, class TEnd = TBegin>
    struct EasyRange {
        TBegin _begin;
        TEnd _end;
        TBegin begin() { return _begin; }
        TBegin end() { return _end; }
    };
    template <class T>
    EasyRange(T&& b, T&& e)->EasyRange<std::remove_cv_t<std::remove_reference_t<T>>>;
}

#endif
