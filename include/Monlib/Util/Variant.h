#ifndef MONLIB_UTIL_VARIANT_H
#define MONLIB_UTIL_VARIANT_H

namespace Monlib::Util {
    template <class... Ts>
    struct overloaded : Ts... {
        using Ts::operator()...;
    };
    template <class... Ts>
    overloaded(Ts...)->overloaded<Ts...>;
}

#endif
