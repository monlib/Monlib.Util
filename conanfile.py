import os

from conans import ConanFile, tools

class MonlibutilConan(ConanFile):
    name = "Monlib.Util"
    license = "GPLv3"
    url = "https://gitlab.com/monlib/Monlib.Util.git"
    description = "Internal Utility Package"
    exports_sources = 'include/*'
    no_copy_source = True
    requires = (
        'gsl_microsoft/20180102@bincrafters/stable',
    )

    def package(self):
        self.copy("*.h")

    def package_id(self):
        self.info.header_only()
