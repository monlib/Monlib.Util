# Monlib.Util

Utility package for C++ Monlib implementations.

## License

GPLv3 or higher

## Versioning

New release -> new version number. This package isn't very well tested and not
meant to be used by outside devs.
